package auth

import "github.com/golang-jwt/jwt/v4"

type Claims struct {
	UID      string   `json:"uid"`
	SID      string   `json:"sid"`
	Publish  bool     `json:"publish"`
	Subcribe bool     `json:"subscribe"`
	Services []string `json:"services"`
	jwt.StandardClaims
}
